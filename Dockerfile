FROM golang:1.13

WORKDIR /go/src/gitlab.com/moemoe89/go-assignment

COPY . .

RUN go mod download
RUN go install

ENTRYPOINT /go/bin/go-assignment -env=docker -migration=up
