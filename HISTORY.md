# Changelog

# Tu, 09 June 2020 | v1.0.0

Initial Features:

- Create knight
- Get knight by id
- Get all knights
- Fight knight

API Documentation:

- Swagger

Test:

- Integration test with dockertest

Container:

- Docker with compose

Version control:

- Git

Programming Language:

- Golang with gorilla/mux

Software Structure:

- Clean architecture

Database:

- PostgreSQL