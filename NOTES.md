# Notes

Changelog of application can be found here:

- [HISTORY.md](./HISTORY.md)

All configurations located on:

- `config.toml`

Arguments available on running application:

- `-env` (local/docker/etc) based on `config.toml` file
- `-migration` (up/down)

How to run on local machine:

- modify `port` value on `config.toml` based on your **available port**
- modify `dsn` value on `config.toml` based on your **PostgreSQL config**
- `go build` then `./go-assignment -env=local -migration=up` for running the application
- or run directly using `go run main.go -env=local -migration=up` command


How to run on docker:

- modify `port` value on `config.toml` also on `docker-compose.yml` based on your **available port**
- modify `dsn` value on `config.toml` based on your **PostgreSQL config** or **PostgreSQL config on Docker**
- `docker-compose up` or `docker-compose up -d` for running the application

How to run test:

- modify `dockertest` value on `config.toml` to true if you **want** to use `dockertest`
- modify `db_port` value on `config.toml` based on your **available port** for exposed db port
- modify `dockertest` value on `config.toml` to false if you **don't want** to use `dockertest`
- modify `dsn` value on `config.toml` if you want to use your own database without `dockertest
- `docker pull postgres:12.3` for pull postgres docker image and made better test preparation
- `go test -p 1 ./...` for running the test

Adding more env:

- modify `config.toml` file and adding another block configuration for the example `[dev]` or `[staging]` or `[prod]`
- run the application using argument with your env for the example `-env=prod`

API Documentation:

- Swagger documentation can be found on this link: `{url}/documentation/index.html`
- The command for generating the docs is `swag init` using this lib: `github.com/swaggo/swag`
- All swagger materials written above `main` and `handler` func using comment block

Changes:

- No major changes for the software structure
- Knight repository minor changes on the return value for supporting getting database error
- Modify initiate provide return value for matching with dockertest behaviour
- Engine minor changes on the return value for supporting http status code
- Engine fight minor changes on input value using struct for easily validating data
- Centralize struct into models for easy management
- Modify initiate adapter input value for achieving calling router on adapter test
- Adding some test for increasing code coverage
