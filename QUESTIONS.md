# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**
   - In my opinion, the project structure is good. Clean architecture concept already adapted on this structure, although every engineer has their style for putting their layer, it's has the same goal and still understable.



 - **What you will improve from your solution ?**
    - Need to add unit test for covering code that uncovered with integration test.
    - Swagger doesn't have a sample value, just the default value based on data type of struct. Need to define later for clarify the data.
    - Need a good migration.
    - If needed, improve the database connection for supporting read and write split connection.



 - **For you, what are the boundaries of a service inside a "micro-service" architecture ?**
    - Instead of the size, I think it's easier when we starting from domain model. With this design, we can grouping and limiting what the area of microservice will be serve. Also we can easily define where we need to put our new feature, into existing microservice or created a new one.



 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store databases ?**
    - SQL nice for the data that need a complex queries and reports. Also if we want to implement ACID for ensure data integrity.
    - NoSQL good for the flexible schema, a large data set.
    - Key-value is really fast and it is good for cache implementation. Then, we can put key value database together with SQL or NoSQL. The request will be come to key-value database first before going to SQL or NoSQL database.
    - Document store, is it different with NoSQL? In my opinion it is same with NoSQL. So, I don't have any clue for answering this. Sorry.


