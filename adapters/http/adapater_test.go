//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package http

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/moemoe89/go-assignment/config"
	"gitlab.com/moemoe89/go-assignment/engine"
	"gitlab.com/moemoe89/go-assignment/providers/database"

	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
)

var (
	router http.Handler
	ID1    string
	ID2    string
)

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	c := config.NewConfig("../..", "test")

	var provider *database.Provider
	var resource *dockertest.Resource

	if c.Dockertest {
		opts := dockertest.RunOptions{
			Repository:   "postgres",
			Tag:          "12.3",
			Env:          []string{"POSTGRES_PASSWORD=" + c.DBPassword},
			ExposedPorts: []string{"5432"},
			PortBindings: map[docker.Port][]docker.PortBinding{
				"5432": {
					{HostIP: "0.0.0.0", HostPort: c.DBPort},
				},
			},
		}

		resource, err = pool.RunWithOptions(&opts)
		if err != nil {
			log.Fatalf("Could not start resource: %s", err)
		}

		c.Dsn = strings.Replace(c.Dsn, "{password}", c.DBPassword, -1)
		c.Dsn = strings.Replace(c.Dsn, "{port}", c.DBPort, -1)
		if err = pool.Retry(func() error {
			provider, err = database.NewProvider(c.Dialect, c.Dsn, c.IdleConn, c.MaxConn)
			return err
		}); err != nil {
			log.Fatalf("Could not connect to docker: %s", err)
		}
	} else {
		provider, err = database.NewProvider(c.Dialect, c.Dsn, c.IdleConn, c.MaxConn)
	}

	err = provider.Drop()
	if err != nil {
		panic(err)
	}

	err = provider.Up()
	if err != nil {
		panic(err)
	}

	e := engine.NewEngine(provider)

	router = GetRouter(e)

	code := m.Run()
	provider.Close()

	if c.Dockertest {
		if err := pool.Purge(resource); err != nil {
			log.Fatalf("Could not purge resource: %s", err)
		}
	}

	os.Exit(code)
}

func TestPostKnightBipolelm(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, "/knight", bytes.NewBuffer([]byte(`{"name":"Bipolelm","strength":10,"weapon_power":20}`)))
	req.Header.Add("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusCreated {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusCreated)
	}
}

func TestPostKnightElrynd(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, "/knight", bytes.NewBuffer([]byte(`{"name":"Elrynd","strength":10,"weapon_power":50}`)))
	req.Header.Add("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusCreated {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusCreated)
	}
}

func TestPostKnightBadData(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, "/knight", bytes.NewBuffer([]byte(`{"name":"FAILED"}`)))
	req.Header.Add("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusBadRequest {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusBadRequest)
	}

	response := map[string]interface{}{}

	if err := json.NewDecoder(recorder.Body).Decode(&response); err != nil {
		t.Fatal(err)
	}

	if _, has := response["code"]; !has {
		t.Fatal("Response error: Expected code field")
	}

	if _, has := response["message"]; !has {
		t.Fatal("Response error: Expected message field")
	}
}

func TestPostKnightBadType(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, "/knight", bytes.NewBuffer([]byte(`name:"Bipolelm"`)))
	req.Header.Add("Content-Type", "text/plain")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusBadRequest {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusBadRequest)
	}
}

func TestGetKnights(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/knight", nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusOK)
	}

	var response []map[string]interface{}

	if err := json.NewDecoder(recorder.Body).Decode(&response); err != nil {
		t.Fatal(err)
	}

	if len(response) != 2 {
		t.Fatal("Response error: Expected 2 knights")
	}

	knight := response[0]

	if _, has := knight["id"]; !has {
		t.Fatal("Response error: Expected id field in knight object")
	}
	if _, has := knight["name"]; !has {
		t.Fatal("Response error: Expected name field in knight object")
	}
	if _, has := knight["strength"]; !has {
		t.Fatal("Response error: Expected strength field in knight object")
	}
	if _, has := knight["weapon_power"]; !has {
		t.Fatal("Response error: Expected weapon_power field in knight object")
	}

	if response[0]["id"].(string) == response[1]["id"].(string) {
		t.Fatal("Response error: Expected not same id for each knights")
	}

	ID1 = response[0]["id"].(string)
	ID2 = response[1]["id"].(string)
}

func TestGetKnightNotFound(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/knight/123456789", nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusNotFound {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusNotFound)
	}

	response := map[string]interface{}{}

	if err := json.NewDecoder(recorder.Body).Decode(&response); err != nil {
		t.Fatal(err)
	}

	if _, has := response["code"]; !has {
		t.Fatal("Response error: Expected code field")
	}

	if _, has := response["message"]; !has {
		t.Fatal("Response error: Expected message field")
	}

	if response["message"].(string) != "Knight #123456789 not found." {
		t.Fatal("Response error: Expected error message 'Knight #123456789 not found.'")
	}
}

func TestGetKnight(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/knight/"+ID1, nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusOK)
	}

	response := map[string]interface{}{}

	if err := json.NewDecoder(recorder.Body).Decode(&response); err != nil {
		t.Fatal(err)
	}

	if _, has := response["id"]; !has {
		t.Fatal("Response error: Expected id field in knight object")
	}
	if _, has := response["name"]; !has {
		t.Fatal("Response error: Expected name field in knight object")
	}
	if _, has := response["strength"]; !has {
		t.Fatal("Response error: Expected strength field in knight object")
	}
	if _, has := response["weapon_power"]; !has {
		t.Fatal("Response error: Expected weapon_power field in knight object")
	}

	if response["id"].(string) != ID1 {
		t.Fatal("Response error: Expected returned ID ", ID1, " instead of ", response["id"].(string))
	}
}

func TestPing(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/ping", nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusOK)
	}

	response := map[string]interface{}{}

	if err := json.NewDecoder(recorder.Body).Decode(&response); err != nil {
		t.Fatal(err)
	}

	if _, has := response["message"]; !has {
		t.Fatal("Response error: Expected message field in ping object")
	}
	if _, has := response["start_time"]; !has {
		t.Fatal("Response error: Expected start_time field in ping object")
	}

	message := "pong"
	if response["message"].(string) != message {
		t.Fatal("Response error: Expected returned message ", message, " instead of ", response["message"].(string))
	}
}

func TestFight(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, "/fight", bytes.NewBuffer([]byte(`{"fighter_1_id":"`+ID1+`","fighter_2_id":"`+ID2+`"}`)))
	req.Header.Add("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusOK)
	}
}

func TestFightBadData(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, "/fight", bytes.NewBuffer([]byte(`{"fighter_1_id":"FAILED"}`)))
	req.Header.Add("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusBadRequest {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusBadRequest)
	}

	response := map[string]interface{}{}

	if err := json.NewDecoder(recorder.Body).Decode(&response); err != nil {
		t.Fatal(err)
	}

	if _, has := response["code"]; !has {
		t.Fatal("Response error: Expected code field")
	}

	if _, has := response["message"]; !has {
		t.Fatal("Response error: Expected message field")
	}
}

func TestFightBadType(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, "/fight", bytes.NewBuffer([]byte(`fighter_1_id:"`+ID1+`"`)))
	req.Header.Add("Content-Type", "text/plain")
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusBadRequest {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusBadRequest)
	}
}
