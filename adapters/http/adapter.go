//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package http

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/handlers"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// HTTPAdapter represents the http server, port and log variables
type HTTPAdapter struct {
	srv  *http.Server
	port int
	log  *logrus.Entry
}

// Start attaches the http adapter and starts listening and serving HTTP requests
func (adapter *HTTPAdapter) Start() {
	if err := adapter.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		adapter.log.Fatalf("Could not listen on %d: %v\n", adapter.port, err)
	}
}

// Stop attaches the http adapter and stop listening and serving HTTP requests
func (adapter *HTTPAdapter) Stop() {
	adapter.srv.SetKeepAlivesEnabled(false)
	if err := adapter.srv.Shutdown(context.Background()); err != nil {
		adapter.log.Fatalf("Could not gracefully shutdown the server: %v\n", err)
	}
}

// NewHTTPAdapter will create a variable that represent the HTTPAdapter struct
func NewHTTPAdapter(h http.Handler, port int, log *logrus.Entry) *HTTPAdapter {
	server := &http.Server{
		Addr:         fmt.Sprintf(":%d", port),
		Handler:      handlers.CORS()(h),
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	return &HTTPAdapter{server, port, log}
}
