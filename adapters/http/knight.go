//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package http

import (
	"encoding/json"
	"net/http"

	"gitlab.com/moemoe89/go-assignment/engine"
	"gitlab.com/moemoe89/go-assignment/models/request"
	"gitlab.com/moemoe89/go-assignment/models/response"

	"github.com/gorilla/mux"
)

//
// @Summary Get Knight
// @Description get knight data
// @Produce  application/json
// @Param id path string true "Knight ID"
// @Success 200 {object} response.Knight
// @Success 404 {object} response.Error
// @Success 500 {object} response.Error
// @Router /knight/{id} [get]
func getKnightHandler(e engine.Engine) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		id := mux.Vars(r)["id"]

		knight, code, err := e.GetKnight(id)
		if err != nil {
			w.WriteHeader(code)
			json.NewEncoder(w).Encode(response.Error{
				Code:    code,
				Message: err.Error(),
			})
			return
		}

		w.WriteHeader(code)
		json.NewEncoder(w).Encode(knight)
	}
}

//
// @Summary Create Knight
// @Description create knight data
// @Accept  json
// @Produce  json
// @Param body body request.KnightRequest true "Request Payload"
// @Success 201 {object} response.Knight
// @Failure 400 {object} response.Error
// @Failure 500 {object} response.Error
// @Router /knight [post]
func createKnightHandler(e engine.Engine) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		req := &request.KnightRequest{}
		err := json.NewDecoder(r.Body).Decode(req)
		if err != nil {
			code := http.StatusBadRequest
			w.WriteHeader(code)
			json.NewEncoder(w).Encode(response.Error{
				Code:    code,
				Message: err.Error(),
			})
			return
		}

		knight, code, err := e.CreateKnight(req)
		if err != nil {
			w.WriteHeader(code)
			json.NewEncoder(w).Encode(response.Error{
				Code:    code,
				Message: err.Error(),
			})
			return
		}

		w.WriteHeader(code)
		json.NewEncoder(w).Encode(knight)
	}
}

//
// @Summary List All Knights
// @Description get list all knights data
// @Produce  application/json
// @Success 200 {array} response.Knight
// @Success 500 {object} response.Error
// @Router /knight [get]
func listKnightHandler(e engine.Engine) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		knights, code, err := e.ListKnights()
		if err != nil {
			w.WriteHeader(code)
			json.NewEncoder(w).Encode(response.Error{
				Code:    code,
				Message: err.Error(),
			})
			return
		}

		w.WriteHeader(code)
		json.NewEncoder(w).Encode(knights)
	}
}

//
// @Summary Fight Knights
// @Description fight knights data
// @Accept  json
// @Produce  json
// @Param body body request.FightRequest true "Request Payload"
// @Success 200 {object} response.Player
// @Failure 400 {object} response.Error
// @Failure 404 {object} response.Error
// @Failure 500 {object} response.Error
// @Router /fight [post]
func fightKnightHandler(e engine.Engine) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		req := &request.FightRequest{}
		err := json.NewDecoder(r.Body).Decode(req)
		if err != nil {
			code := http.StatusBadRequest
			w.WriteHeader(code)
			json.NewEncoder(w).Encode(response.Error{
				Code:    code,
				Message: err.Error(),
			})
			return
		}

		res, code, err := e.Fight(req)
		if err != nil {
			w.WriteHeader(code)
			json.NewEncoder(w).Encode(response.Error{
				Code:    code,
				Message: err.Error(),
			})
			return
		}

		w.WriteHeader(code)
		json.NewEncoder(w).Encode(response.Player{
			ID:       res.GetID(),
			Strength: res.GetPower(),
		})
	}
}
