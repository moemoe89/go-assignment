//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package http

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/moemoe89/go-assignment/config"
	"gitlab.com/moemoe89/go-assignment/engine"
	"gitlab.com/moemoe89/go-assignment/models/response"
)

// starTime will be init date time data when starting the service
var starTime = time.Now()

//
// @Summary Ping
// @Description get service status
// @Produce  application/json
// @Success 200 {object} response.Ping
// @Router /ping [get]
func pingHandler(e engine.Engine) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		loc, _ := time.LoadLocation("Asia/Tokyo")
		startTime := starTime.In(loc)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(response.Ping{
			Version:   config.GetVersion(),
			Env:       config.GetEnv(),
			Message:   "pong",
			StartTime: startTime.Format("[02 January 2006] 15:04:05 MST"),
		})
	}
}
