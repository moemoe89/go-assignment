//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package http

import (
	"net/http"

	_ "gitlab.com/moemoe89/go-assignment/docs"
	"gitlab.com/moemoe89/go-assignment/engine"

	"github.com/gorilla/mux"
	"github.com/swaggo/http-swagger"
)

// GetRouter will create a variable that represent the mux.Router
func GetRouter(e engine.Engine) *mux.Router {
	r := mux.NewRouter().StrictSlash(false)
	r.NewRoute().
		Name("Ping").
		Methods(http.MethodGet).
		Path("/ping").
		Handler(pingHandler(e))
	r.NewRoute().
		Name("Create knight").
		Methods(http.MethodPost).
		Path("/knight").
		Handler(createKnightHandler(e))
	r.NewRoute().
		Name("Get knight").
		Methods(http.MethodGet).
		Path("/knight/{id}").
		Handler(getKnightHandler(e))
	r.NewRoute().
		Name("List all knights").
		Methods(http.MethodGet).
		Path("/knight").
		Handler(listKnightHandler(e))
	r.NewRoute().
		Name("Fight knights").
		Methods(http.MethodPost).
		Path("/fight").
		Handler(fightKnightHandler(e))
	r.NewRoute().
		Name("Swagger documentation").
		Methods(http.MethodGet).
		PathPrefix("/documentation/").
		Handler(httpSwagger.WrapHandler)

	return r
}
