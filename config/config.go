//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package config

import (
	"fmt"

	"github.com/spf13/viper"
)

var (
	versionGlobal string
	envGlobal     string
)

// Configuration represent the configuration model
type Configuration struct {
	Port       int
	Dialect    string
	Dsn        string
	IdleConn   int
	MaxConn    int
	Dockertest bool
	DBPassword string
	DBPort     string
}

// NewConfig will create a variable that represent the Configuration struct
func NewConfig(configPath, env string) *Configuration {
	viper.SetConfigName("config")
	viper.SetConfigType("toml")
	viper.AddConfigPath(configPath)
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	if len(env) == 0 {
		env = "local"
	}

	keys := viper.AllSettings()
	if _, ok := keys[env]; !ok {
		panic(fmt.Errorf("Env not match. Please check config.toml file"))
	}

	envGlobal = env
	versionGlobal = viper.GetString("version")
	port := viper.GetInt(env + ".port")
	dialect := viper.GetString(env + ".dialect")
	dsn := viper.GetString(env + ".dsn")
	idleConn := viper.GetInt(env + ".idle_conn")
	maxConn := viper.GetInt(env + ".max_conn")
	dockertest := viper.GetBool(env + ".dockertest")
	dbPassword := viper.GetString(env + ".db_password")
	dbPort := viper.GetString(env + ".db_port")

	return &Configuration{
		Port:       port,
		Dialect:    dialect,
		Dsn:        dsn,
		IdleConn:   idleConn,
		MaxConn:    maxConn,
		Dockertest: dockertest,
		DBPassword: dbPassword,
		DBPort:     dbPort,
	}
}

// GetVersion return the version of service
func GetVersion() string {
	return versionGlobal
}

// GetEnv return the env of service
func GetEnv() string {
	return envGlobal
}
