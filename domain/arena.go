//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package domain

import "gitlab.com/moemoe89/go-assignment/models/response"

// Arena represent the arena model
type Arena struct{}

// Fight attaches the arena and comparing power between two fighter
func (arena *Arena) Fight(fighter1 interface{}, fighter2 interface{}) Fighter {
	f1 := fighter1.(Fighter)
	f2 := fighter2.(Fighter)

	if f1.GetPower() < f2.GetPower() {
		return &response.Player{f2.GetID(), f2.GetPower()}
	} else if f1.GetPower() > f2.GetPower() {
		return &response.Player{f1.GetID(), f1.GetPower()}
	}
	return nil
}
