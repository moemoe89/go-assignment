//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package domain

// Fighter represent the fighter behaviour
type Fighter interface {
	GetID() string
	GetPower() float64
}
