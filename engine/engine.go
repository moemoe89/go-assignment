//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package engine

import (
	"gitlab.com/moemoe89/go-assignment/domain"
	"gitlab.com/moemoe89/go-assignment/models/request"
	"gitlab.com/moemoe89/go-assignment/models/response"
)

// Engine represent the engine behaviour
type Engine interface {
	GetKnight(ID string) (*response.Knight, int, error)
	ListKnights() ([]*response.Knight, int, error)
	Fight(req *request.FightRequest) (domain.Fighter, int, error)
	CreateKnight(req *request.KnightRequest) (*response.Knight, int, error)
}

// KnightRepository represent the knight repository behaviour
type KnightRepository interface {
	Find(ID string) (*response.Knight, error)
	FindAll() ([]*response.Knight, error)
	Save(knight *response.Knight) error
}

// DatabaseProvider represent the database provider behaviour
type DatabaseProvider interface {
	GetKnightRepository() KnightRepository
}

// arenaEngine represent the arena engine model
type arenaEngine struct {
	arena            *domain.Arena
	knightRepository KnightRepository
}

// NewEngine will create a variable that represent the Engine behaviour
func NewEngine(db DatabaseProvider) Engine {
	return &arenaEngine{
		arena:            &domain.Arena{},
		knightRepository: db.GetKnightRepository(),
	}
}
