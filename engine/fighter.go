//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package engine

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/moemoe89/go-assignment/domain"
	"gitlab.com/moemoe89/go-assignment/models/request"
	"gitlab.com/moemoe89/go-assignment/models/response"

	"github.com/google/uuid"
)

// GetKnight attaches the arena engine and find data based on id
func (engine *arenaEngine) GetKnight(ID string) (*response.Knight, int, error) {
	knight, err := engine.knightRepository.Find(ID)
	if err == sql.ErrNoRows {
		return nil, http.StatusNotFound, errors.New(fmt.Sprintf("Knight #%s not found.", ID))
	}

	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return knight, http.StatusOK, nil
}

// ListKnights attaches the arena engine and find all data
func (engine *arenaEngine) ListKnights() ([]*response.Knight, int, error) {
	knights, err := engine.knightRepository.FindAll()
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return knights, http.StatusOK, nil
}

// Fight attaches the arena engine and fight between two fighter
func (engine *arenaEngine) Fight(req *request.FightRequest) (domain.Fighter, int, error) {
	if req.Validate() != nil {
		return nil, http.StatusBadRequest, req.Validate()
	}

	fighter1, err := engine.knightRepository.Find(req.Fighter1ID)
	if err == sql.ErrNoRows {
		return nil, http.StatusNotFound, errors.New(fmt.Sprintf("Knight #%s not found.", req.Fighter1ID))
	}

	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	fighter2, err := engine.knightRepository.Find(req.Fighter2ID)
	if err == sql.ErrNoRows {
		return nil, http.StatusNotFound, errors.New(fmt.Sprintf("Knight #%s not found.", req.Fighter2ID))
	}

	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	result := engine.arena.Fight(fighter1, fighter2)

	return result, http.StatusOK, nil
}

// CreateKnight attaches the arena engine and creating the data
func (engine *arenaEngine) CreateKnight(req *request.KnightRequest) (*response.Knight, int, error) {
	if req.Validate() != nil {
		return nil, http.StatusBadRequest, req.Validate()
	}

	knight := &response.Knight{
		ID:          uuid.New().String(),
		Name:        req.Name,
		Strength:    req.Strength,
		WeaponPower: req.WeaponPower,
	}
	err := engine.knightRepository.Save(knight)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return knight, http.StatusCreated, nil
}
