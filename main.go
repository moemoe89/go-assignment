//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/moemoe89/go-assignment/adapters/http"
	"gitlab.com/moemoe89/go-assignment/config"
	"gitlab.com/moemoe89/go-assignment/engine"
	"gitlab.com/moemoe89/go-assignment/logger"
	"gitlab.com/moemoe89/go-assignment/providers/database"
)

// @title Go Assignment
// @version 1.0.0
// @description This is a doc for Go Assignment.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email bismobaruno@gmail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host
// @BasePath /
func main() {
	env := ""
	migration := ""

	flag.StringVar(&env, "env", "", "Env option")
	flag.StringVar(&migration, "migration", "", "Migration option")
	flag.Parse()

	c := config.NewConfig(".", env)

	provider, err := database.NewProvider(c.Dialect, c.Dsn, c.IdleConn, c.MaxConn)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer func() {
		provider.Close()
	}()

	if migration == "up" {
		log.Println("Running migration, create table...")
		provider.Up()
	}

	if migration == "down" {
		log.Println("Running migration, drop table...")
		provider.Drop()
	}

	e := engine.NewEngine(provider)

	p := c.Port
	r := http.GetRouter(e)
	l := logger.NewLogger()
	adapter := http.NewHTTPAdapter(r, p, l)

	done := make(chan bool)
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-stop
		log.Println("Server is shutting down...")

		adapter.Stop()
		provider.Close()
		close(done)
	}()

	log.Printf("Server is ready to handle requests at :%d", p)
	adapter.Start()

	<-done
	log.Println("Server stopped")
}
