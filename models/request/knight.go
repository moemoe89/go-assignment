//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package request

import (
	"errors"
)

type (
	// KnightRequest represent the knight request model
	KnightRequest struct {
		Name        string `json:"name"`
		Strength    int    `json:"strength"`
		WeaponPower int    `json:"weapon_power"`
	}

	// FightRequest represent the fight request model
	FightRequest struct {
		Fighter1ID string `json:"fighter_1_id"`
		Fighter2ID string `json:"fighter_2_id"`
	}
)

// Validate attaches the knight request and validating data
func (k *KnightRequest) Validate() error {
	if len(k.Name) == 0 {
		return errors.New("name can't be empty")
	}

	if k.Strength < 1 {
		return errors.New("strength can't be empty")
	}

	if k.WeaponPower < 1 {
		return errors.New("weapon_power can't be empty")
	}

	return nil
}

// Validate attaches the fight request and validating data
func (f *FightRequest) Validate() error {
	if len(f.Fighter1ID) == 0 {
		return errors.New("fighter_1_id can't be empty")
	}

	if len(f.Fighter2ID) == 0 {
		return errors.New("fighter_2_id can't be empty")
	}

	return nil
}
