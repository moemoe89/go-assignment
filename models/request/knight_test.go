//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package request

import "testing"

func TestKnightValidationName(t *testing.T) {
	req := KnightRequest{}
	err := req.Validate()

	if err.Error() != "name can't be empty" {
		t.Error("validation result is not the name")
	}
}

func TestKnightValidationStrength(t *testing.T) {
	req := KnightRequest{
		Name: "momo",
	}
	err := req.Validate()

	if err.Error() != "strength can't be empty" {
		t.Error("validation result is not the strength")
	}
}

func TestKnightValidationWeaponPower(t *testing.T) {
	req := KnightRequest{
		Name:     "momo",
		Strength: 100,
	}
	err := req.Validate()

	if err.Error() != "weapon_power can't be empty" {
		t.Error("validation result is not the weapon_power")
	}
}

func TestKnightValidation(t *testing.T) {
	req := KnightRequest{
		Name:        "momo",
		Strength:    100,
		WeaponPower: 50,
	}
	err := req.Validate()

	if err != nil {
		t.Error("validation result is nil")
	}
}

func TestFightValidationFighter1(t *testing.T) {
	req := FightRequest{}
	err := req.Validate()

	if err.Error() != "fighter_1_id can't be empty" {
		t.Error("validation result is not the fighter_1_id")
	}
}

func TestFightValidationFighter2(t *testing.T) {
	req := FightRequest{
		Fighter1ID: "1",
	}
	err := req.Validate()

	if err.Error() != "fighter_2_id can't be empty" {
		t.Error("validation result is not the fighter_2_id")
	}
}

func TestFightValidation(t *testing.T) {
	req := FightRequest{
		Fighter1ID: "1",
		Fighter2ID: "2",
	}
	err := req.Validate()

	if err != nil {
		t.Error("validation result is nil")
	}
}
