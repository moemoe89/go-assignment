//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package response

type (
	// Error represent the error model
	Error struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}
)
