//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package response

type (
	// Knight represent the knight model
	Knight struct {
		ID          string `json:"id"`
		Name        string `json:"name"`
		Strength    int    `json:"strength"`
		WeaponPower int    `json:"weapon_power"`
	}

	// Player represent the player model
	Player struct {
		ID       string
		Strength float64
	}
)

// GetID attaches the knight and getting id data
func (k *Knight) GetID() string {
	return k.ID
}

// GetPower attaches the knight and getting power data
func (k *Knight) GetPower() float64 {
	return float64(k.Strength + k.WeaponPower)
}

// GetID attaches the player and getting id data
func (p *Player) GetID() string {
	return p.ID
}

// GetPower attaches the player and getting power data
func (p *Player) GetPower() float64 {
	return p.Strength
}
