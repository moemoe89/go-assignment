//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package response

import "testing"

func TestKnightGetID(t *testing.T) {
	id := "1"
	res := Knight{
		ID: id,
	}
	getID := res.GetID()

	if id != getID {
		t.Error("expected returned ID ", id, " insted of ", getID)
	}
}

func TestKnightGetPower(t *testing.T) {
	strength := 50
	weaponPower := 50
	power := strength + weaponPower
	res := Knight{
		Strength:    strength,
		WeaponPower: weaponPower,
	}
	getPower := res.GetPower()

	if float64(power) != getPower {
		t.Error("expected power ", power, " insted of ", getPower)
	}
}

func TestPlayerGetID(t *testing.T) {
	id := "1"
	res := Player{
		ID: id,
	}
	getID := res.GetID()

	if id != getID {
		t.Error("expected returned ID ", id, " insted of ", getID)
	}
}

func TestPlayerGetPower(t *testing.T) {
	power := float64(50)
	res := Player{
		Strength: power,
	}
	getPower := res.GetPower()

	if power != getPower {
		t.Error("expected power ", power, " insted of ", getPower)
	}
}
