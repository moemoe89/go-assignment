//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package response

type (
	// Ping represent the ping model
	Ping struct {
		Version   string `json:"version"`
		Env       string `json:"env"`
		Message   string `json:"message"`
		StartTime string `json:"start_time"`
	}
)
