//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package database

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/moemoe89/go-assignment/models/response"
)

// knightRepository represent the knight repository model
type knightRepository struct {
	db *sql.DB
}

// Find attaches the knight repository and find data based on id
func (repository *knightRepository) Find(ID string) (*response.Knight, error) {
	knight := new(response.Knight)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := repository.db.QueryRowContext(ctx, "SELECT id, name, strength, weapon_power FROM knights WHERE id = $1", ID).Scan(&knight.ID, &knight.Name, &knight.Strength, &knight.WeaponPower)
	if err != nil {
		return nil, err
	}
	return knight, nil
}

// FindAll attaches the knight repository and find all data
func (repository *knightRepository) FindAll() ([]*response.Knight, error) {
	knights := make([]*response.Knight, 0)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	rows, err := repository.db.QueryContext(ctx, "SELECT id, name, strength, weapon_power FROM knights")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		knight := new(response.Knight)
		err = rows.Scan(
			&knight.ID,
			&knight.Name,
			&knight.Strength,
			&knight.WeaponPower,
		)

		if err != nil {
			return nil, err
		}
		knights = append(knights, knight)
	}

	return knights, nil
}

// Save attaches the knight repository and creating the data
func (repository *knightRepository) Save(knight *response.Knight) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	query := "INSERT INTO knights (id, name, strength, weapon_power) VALUES ($1, $2, $3, $4)"
	stmt, err := repository.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, knight.ID, knight.Name, knight.Strength, knight.WeaponPower)
	return err
}
