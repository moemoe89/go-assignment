//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package database

import (
	"log"
	"os"
	"strings"
	"testing"

	"gitlab.com/moemoe89/go-assignment/config"
	"gitlab.com/moemoe89/go-assignment/engine"
	"gitlab.com/moemoe89/go-assignment/models/response"

	"github.com/google/uuid"
	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
	"github.com/stretchr/testify/assert"
)

var (
	knightRepositoryTest engine.KnightRepository
)

var knight = &response.Knight{
	ID:          uuid.New().String(),
	Name:        "Lothar",
	Strength:    40,
	WeaponPower: 30,
}

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	c := config.NewConfig("../..", "test")

	var provider *Provider
	var resource *dockertest.Resource

	if c.Dockertest {
		opts := dockertest.RunOptions{
			Repository:   "postgres",
			Tag:          "12.3",
			Env:          []string{"POSTGRES_PASSWORD=" + c.DBPassword},
			ExposedPorts: []string{"5432"},
			PortBindings: map[docker.Port][]docker.PortBinding{
				"5432": {
					{HostIP: "0.0.0.0", HostPort: c.DBPort},
				},
			},
		}

		resource, err = pool.RunWithOptions(&opts)
		if err != nil {
			log.Fatalf("Could not start resource: %s", err)
		}

		c.Dsn = strings.Replace(c.Dsn, "{password}", c.DBPassword, -1)
		c.Dsn = strings.Replace(c.Dsn, "{port}", c.DBPort, -1)
		if err = pool.Retry(func() error {
			provider, err = NewProvider(c.Dialect, c.Dsn, c.IdleConn, c.MaxConn)
			return err
		}); err != nil {
			log.Fatalf("Could not connect to docker: %s", err)
		}
	} else {
		provider, err = NewProvider(c.Dialect, c.Dsn, c.IdleConn, c.MaxConn)
	}

	defer func() {
		provider.Close()
	}()

	err = provider.Drop()
	if err != nil {
		panic(err)
	}

	err = provider.Up()
	if err != nil {
		panic(err)
	}

	knightRepositoryTest = provider.GetKnightRepository()

	code := m.Run()

	if c.Dockertest {
		if err := pool.Purge(resource); err != nil {
			log.Fatalf("Could not purge resource: %s", err)
		}
	}

	os.Exit(code)
}

func TestSaveKnight(t *testing.T) {
	err := knightRepositoryTest.Save(knight)
	assert.NoError(t, err)
}

func TestFindKnight(t *testing.T) {
	res, err := knightRepositoryTest.Find(knight.ID)
	assert.NotEmpty(t, res)
	assert.NoError(t, err)
	assert.Equal(t, knight.ID, res.ID)
	assert.Equal(t, knight.Name, res.Name)
	assert.Equal(t, knight.Strength, res.Strength)
	assert.Equal(t, knight.WeaponPower, res.WeaponPower)
}

func TestFindAllKnight(t *testing.T) {
	knights, err := knightRepositoryTest.FindAll()
	assert.NotEmpty(t, knights)
	assert.NoError(t, err)
	assert.NotEqual(t, 0, len(knights))
}
