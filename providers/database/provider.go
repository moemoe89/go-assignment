//
//  go-assignment
//
//  Copyright © 2020. All rights reserved.
//

package database

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"gitlab.com/moemoe89/go-assignment/engine"

	_ "github.com/lib/pq"
)

// Provider represent the provider model
type Provider struct {
	db *sql.DB
}

// GetKnightRepository attaches the provider and getting knight respository object
func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return &knightRepository{provider.db}
}

// Close attaches the provider and close the connection
func (provider *Provider) Close() {
	provider.db.Close()
}

// Up attaches the provider and create the table
func (provider *Provider) Up() error {
	ctx := context.Background()

	query :=
		"CREATE TABLE IF NOT EXISTS knights (" +
			"id VARCHAR(36) PRIMARY KEY," +
			"name VARCHAR(50)," +
			"strength INT," +
			"weapon_power INT" +
			")"
	stmt, err := provider.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx)
	if err != nil {
		return err
	}
	return nil
}

// Drop attaches the provider and drop the table
func (provider *Provider) Drop() error {
	ctx := context.Background()

	query := "DROP TABLE IF EXISTS knights"
	stmt, err := provider.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx)
	if err != nil {
		return err
	}
	return nil
}

// NewProvider will create a variable that represent the Provider struct
func NewProvider(dialect, dsn string, idleConn, maxConn int) (*Provider, error) {
	db, err := sql.Open(dialect, dsn)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Failed to open db file: %s", err.Error()))
	}

	err = db.Ping()
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Failed to ping connection to db: %s", err.Error()))
	}

	db.SetMaxIdleConns(idleConn)
	db.SetMaxOpenConns(maxConn)

	return &Provider{db}, nil
}
